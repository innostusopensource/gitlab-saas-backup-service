<?php

class ExportProjects
{
    private array $projectlist;

    private string $authkey;

    public function __construct($authkey) {
        $this->authkey = $authkey;
    }

    function LoadProjects($groups): void
    {
        $this->projectlist = [];

        foreach ($groups as $group)
        {
            $this->GetProjectsFromGroup($group);
        }
    }

    function DownloadAllExports($outputfolder): void
    {
        foreach ($this->projectlist as $project)
        {
            $file = $outputfolder . $project . ".tar.gz";
            $waittime = 30;
            $filewritten=false;

            echo "Starting download for project " . $project . ".\n";

            do
            {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://gitlab.com/api/v4/projects/' . $project . '/export/download',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: Bearer ' . $this->authkey
                    ),
                ));

                sleep(1); //small delay to reduce rate limiting
                $response = curl_exec($curl);
                $result = json_decode($response, true);
                curl_close($curl);

                if (is_array($result))
                {
                    $result = json_decode($response, true);

                    if (array_key_exists("message", $result))
                    {
                        if (is_array($result["message"]))
                        {
                            if (array_key_exists("error", $result["message"]))
                            {
                                if (($result["message"]["error"] == "This endpoint has been requested too many times. Try again later."))
                                {
                                    echo "Ratelimit reached, wait " . $waittime . " seconds.\n";
                                    sleep($waittime);
                                    $waittime *= 2;
                                }
                            }
                        }
                    }
                }
                else
                {
                    file_put_contents($file, $response);
                    $filewritten=true;
                }
            }
            while(!$filewritten);
        }
    }


function WaitForExportsToFinish(): void
{
    foreach ($this->projectlist as $project)
    {
        $waittime = 30;
        do
        {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://gitlab.com/api/v4/projects/' . $project . '/export',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . $this->authkey
                ),
            ));

            sleep(1); //small delay to reduce rate limiting
            $response = curl_exec($curl);

            curl_close($curl);

            $result = json_decode($response, true);

            if ($result["export_status"] != "finished")
            {
                echo "Project " . $project . " not finished yet. Wait " . $waittime . " seconds.\n";
                sleep($waittime);
                $waittime *= 2;
            }
        } while ($result["export_status"] != "finished");
    }
}

function ExportAllProjects(): void
{
    foreach ($this->projectlist as $project)
    {
        $waittime = 30;
        do
        {
            $curl = curl_init();

            echo "Starting export of project " . $project . ".\n";

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://gitlab.com/api/v4/projects/' . $project . '/export',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . $this->authkey
                ),
            ));

            sleep(1); //small delay to reduce rate limiting
            $response = curl_exec($curl);

            curl_close($curl);

            $result = json_decode($response, true);

            if (array_key_exists("message", $result))
            {
                if ($result["message"] != "202 Accepted")
                {
                    if (is_array($result["message"]))
                    {
                        if (array_key_exists("error", $result["message"]))
                        {
                            if (($result["message"]["error"] == "This endpoint has been requested too many times. Try again later."))
                            {
                                echo "Ratelimit reached, wait " . $waittime . " seconds.\n";
                                sleep($waittime);
                                $waittime *= 2;
                            }
                        }
                        else
                        {
                            echo "Failed project:" . $project . ". Unknown error.\n";
                        }
                    }
                    else
                    {
                        echo "Failed project:" . $project . ". Result: " . $result["message"] . "\n";
                    }
                }
            }
            else
            {
                echo "Failed project:" . $project . ". Response: " . $response . "\n";
            }
        } while ($result["message"] != "202 Accepted");
    }
}

function GetProjectsFromGroup($group): void
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://gitlab.com/api/v4/groups/' . $group . '/projects',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . $this->authkey
        ),
    ));

    sleep(1); //small delay to reduce rate limiting
    $response = curl_exec($curl);

    curl_close($curl);
    $result = json_decode($response, true);

    foreach ($result as $project)
    {
        $this->projectlist[] = $project['id'];
        echo "Project: " . $project['id'] . ".\n";
    }
}
}
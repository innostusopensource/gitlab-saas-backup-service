# GitLab SaaS Backup Service

## Description
A php commandline application to automatically trigger and download an export for each project in every group of the authenticated GitLab account. Ideal to be run periodically in cron. Authentication is passed as a commandline argument.

## Installation
Prerequisites: PHP8.1, not tested with other PHP versions, however, they could work.
Download the latest release from this repository.
Extract to a folder of your choice.
Run composer install.
Run as described below.

##Authentication token GitLab
Create a Personal Access Token with 'api' scope.	

## Usage
exportall.php -a gitlabauthenticationkey -o outputfolder

## Support
See GitLab repository.

## Roadmap
See GitLab repository.

## Contributing
See GitLab repository.

## Authors and acknowledgment
Arnold Boer, Innostus, https://www.innostus.com

## License
FreeBSD

## Project status
Passively maintained.

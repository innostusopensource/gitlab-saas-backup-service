<?php
require_once "GetGroups.php";
require_once "ExportProjects.php";
require_once "helpersfuncs.php";

if(!is_cli())
{
    echo "This is a CLI tool. Please run it from the command line.\n";
    exit();
}

if(!isset($argv))
{
    showargs();
    exit();
}

// Script example.php
$options = getopt("a:o:");

if(isset($options["a"])) {
    $authkey = $options["a"];
}
else
{
    showargs();
    exit();
}

if(isset($options["o"])) {
    $outputfolder = $options["o"];
}
else
{
    showargs();
    exit();
}

echo "Retrieving all Groups.\n";
$g = new GetGroups($authkey);
$groups = $g->Get();

if(is_null($groups) || !is_array($groups) || !count($groups) )
{
    echo "Unable to get groups. No groups? Authentication failed?\n";
    exit();
}
$e = new ExportProjects($authkey);
echo "Retreiving a list of Projects from all groups.\n";
$e->LoadProjects($groups);
echo "Sending export commands to all projects.\n";
$e->ExportAllProjects();
echo "Waiting for all exports to finish.\n";
$e->WaitForExportsToFinish();
echo "Downloading the exports.\n";
$e->DownloadAllExports($outputfolder);
echo "Finished!\n";


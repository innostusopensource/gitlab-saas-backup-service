<?php

class GetGroups
{
    private string $authkey;

    public function __construct($authkey) {
        $this->authkey = $authkey;
    }

    function Get(): array
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gitlab.com/api/v4/groups/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $this->authkey
            ),
        ));

        sleep(1); //small delay to reduce rate limiting
        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response, true);

        $groups=[];

        if(isset($result['error']))
        {
            echo $result['error'] . "\n";
            if(isset($result['error_description']))
            {
                echo $result['error_description'] . "\n";
            }
            exit();
        }
        if(isset($result['message']))
        {
            echo $result['message'] . "\n";
        }
        else
        {
            foreach ($result as $group)
            {
                $groups[] = $group['id'];
                echo "Group: " . $group['id'] . ".\n";
            }
        }

        return $groups;
    }
}